var basePath = 'http://192.168.1.44:8083/';
var formidable = require('formidable');
var util = require('util');
var fs   = require('fs-extra');


exports.roomAll = function(req, res){
    var datax=[];

    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM room',function(err,room)
        {
            if(err){
                console.log("Error Selecting : %s ",err );
            }
            else{
                for(var s = 0;s < room.length;s++){
                    datax[s]={'id':room[s].idroom,
                        'name':room[s].nama,
                        'ip':basePath+room[s].img
                    };
                }
                res.json({ room:datax });
            }
        });
    });
};

exports.roomItem = function(req, res){
    console.log(req);
    var datax=[];
    var datax2=[];
    var datax3=[];
    var form = new formidable.IncomingForm();

    form.parse(req, function(err, fields, files) {

        var idRoom = fields.idroom;
        req.getConnection(function(err,connection){
            var query = connection.query('SELECT * FROM room where flag="Y" and idroom= ?',idRoom,function(err,room)
            {
                if(err){
                    console.log("Error Selecting : %s ",err );
                }
                else{
                    for(var s = 0;s < room.length;s++){
                        datax[s]={
                            'id':room[s].idroom,
                            'name':room[s].nama,
                            'img':basePath+room[s].img
                        };
                    }
                    var query = connection.query('SELECT * FROM control_ac where flag="Y" and idroom= ?',idRoom,function(err,room2)
                    {
                        if(err){
                            console.log("Error Selecting : %s ",err );
                        }
                        else{
                            for(var s = 0;s < room2.length;s++){
                                datax2[s]={
                                    'id':room2[s].idac,
                                    'name':room2[s].nama,
                                    'status':room2[s].status,
                                    'mode':room2[s].mode,
                                    'suhuruang':room2[s].suhuruang,
                                    'temp':room2[s].temp,
                                    'fan':room2[s].fan,
                                    'idroom':room2[s].idroom
                                };
                            }
                            var query = connection.query('SELECT * FROM control_light where flag="Y" and idroom= ?',idRoom,function(err,room3)
                            {
                                if(err){
                                    console.log("Error Selecting : %s ",err );
                                }
                                else{
                                    for(var s = 0;s < room3.length;s++){
                                        datax3[s]={
                                            'id':room3[s].idcl,
                                            'idroom':room3[s].idroom,
                                            'level':room3[s].level
                                        };
                                    }
                                    res.json({ room:datax, ac:datax2 , lamp:datax3});
                                }
                            });
                        }
                    });
                }
            });
        });

    });
};



exports.roomSave = function(req, res){
    var form = new formidable.IncomingForm();
    var afields = [];


    form.parse(req, function(err, fields, files) {
        var fields = fields;
        //res.writeHead(200, {'content-type': 'text/plain'});
        //res.write('Upload received :\n');
        //res.end(util.inspect({fields: fields, files: files}));
        afields.push(fields);

    });
    form.on('end', function(fields, files) {
        console.log(afields[0]);
        /* Temporary location of our uploaded file */
        var temp_path = this.openedFiles[0].path;
        /* The file name of the uploaded file */
        var file_name = this.openedFiles[0].name;
        /* Location where we want to copy the uploaded file */
        var new_location = __dirname+'/../public/img/room/';

                req.getConnection(function(err,connection){
                    var query = connection.query("SELECT COUNT(img) as jml FROM room ", function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var nameimgfile = 'gbrupload'+(rows[0].jml+1)+file_name+'.jpeg';
                        var nameimg = 'img/room/gbrupload'+(rows[0].jml+1)+file_name+'.jpeg';
                        fs.copy(temp_path, new_location + nameimgfile, function(err) {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log("success upload!");

                        var datainsert = {
                            nama            : afields[0].nama,
                            img          : nameimg,
                            idfloor          : '1',
                            flag          : 'Y'
                        };
                                var query = connection.query("INSERT INTO room set ? ",datainsert, function(err, rows)
                                {
                                    if(err){
                                        var data = {
                                            result    : 'N',
                                            insert : datainsert

                                        };
                                        res.json({ data:data });
                                        console.log("Error Selecting : %s ",err );
                                    }
                                    else{
                                        var data = {
                                            result    : 'Y',
                                            insert : datainsert
                                        };
                                        res.json({ data:data });
                                    }
                                });
                            }
                        });
                    }
                });
                });

    });

};