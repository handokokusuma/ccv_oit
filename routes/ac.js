var formidable = require('formidable');
var util = require('util');



exports.acAll = function(req, res){
    var datax=[];

    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM control_ac where flag="Y"',function(err,roomL)
        {
            if(err){
                console.log("Error Selecting : %s ",err );
            }
            else{
                for(var s = 0;s < roomL.length;s++){
                    datax[s]={'id':roomL[s].idac,
                        'name':roomL[s].nama,
                        'status':roomL[s].status,
                        'mode':roomL[s].mode,
                        'temp':roomL[s].temp,
                        'fan':roomL[s].fan,
                        'id Room':roomL[s].idroom
                    };
                }
                res.json({ ac:datax });
            }
        });
    });
};

exports.acSave = function(req, res){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var datainsert = {
            nama            : fields.nama,
            status          : fields.status,
            mode          : fields.mode,
            temp          : fields.temp,
            suhuruang      : 28,
            fan          : fields.fan,
            idroom          : fields.idroom,
            flag          : 'Y'
        };
        console.log(datainsert);
        req.getConnection(function(err,connection){
            var query = connection.query("INSERT INTO control_ac set ? ",datainsert, function(err, rows)
            {
                if(err){
                    var data = {
                        result    : 'N',
                        insert : datainsert

                    };
                            res.json({ data:data });
                    console.log("Error Selecting : %s ",err );
                }
                else{
                    var data = {
                        result    : 'Y',
                        insert : datainsert
                    };
                            res.json({ data:data });
                }
            });
            //console.log(query.sql);
        });
    });
};

exports.acShow = function(req, res){
    var id = req.params.id;
    var datax=[];

    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM control_ac where flag="Y" and id = ?',id,function(err,roomL)
        {
            if(err){
                console.log("Error Selecting : %s ",err );
            }
            else{
                for(var s = 0;s < roomL.length;s++){
                    datax[s]={'id':roomL[s].id,
                        'name':roomL[s].nama,
                        'status':roomL[s].status,
                        'mode':roomL[s].mode,
                        'suhuruang':roomL[s].suhuruang,
                        'temp':roomL[s].mode,
                        'fan':roomL[s].mode,
                        'id Room':roomL[s].idroom
                    };
                }
                res.json({ ac:datax });
            }
        });
    });
};

exports.acEditSave = function(req, res){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var id = fields.idroom;

        if(fields.temp){
            var datainsert = {
                temp          : fields.temp
            };
            req.getConnection(function(err,connection){
                var query = connection.query("UPDATE control_ac set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var data = {
                            result    : 'Y',
                            insert : datainsert
                        };
                        res.json({ data:data });
                    }
                });
                //console.log(query.sql);
            });
        }
        else if(fields.status){
            var datainsert = {
                status          : fields.status
            };
            req.getConnection(function(err,connection){
                var query = connection.query("UPDATE control_ac set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var data = {
                            result    : 'Y',
                            insert : datainsert
                        };
                        res.json({ data:data });
                    }
                });
                //console.log(query.sql);
            });
        }
        else if(fields.mode){
            var datainsert = {
                mode          : fields.mode
            };
            req.getConnection(function(err,connection){
                var query = connection.query("UPDATE control_ac set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var data = {
                            result    : 'Y',
                            insert : datainsert
                        };
                        res.json({ data:data });
                    }
                });
                //console.log(query.sql);
            });
        }
        else if(fields.fan){
            var datainsert = {
                fan          : fields.fan
            };
            req.getConnection(function(err,connection){
                var query = connection.query("UPDATE control_ac set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var data = {
                            result    : 'Y',
                            insert : datainsert
                        };
                        res.json({ data:data });
                    }
                });
                //console.log(query.sql);
            });
        }

        else if(fields.suhuruang){
            var datainsert = {
                suhuruang          : fields.suhuruang
            };
            req.getConnection(function(err,connection){
                var query = connection.query("UPDATE control_ac set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
                {
                    if(err){
                        var data = {
                            result    : 'N',
                            insert : datainsert

                        };
                        res.json({ data:data });
                        console.log("Error Selecting : %s ",err );
                    }
                    else{
                        var data = {
                            result    : 'Y',
                            insert : datainsert
                        };
                        res.json({ data:data });
                    }
                });
                //console.log(query.sql);
            });
        }

    });
};

//exports.acEditSave = function(req, res){
//    var id = req.params.id;
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var datainsert = {
//            nama            : fields.nama,
//            status          : fields.status,
//            mode          : fields.mode,
//            temp          : fields.temp,
//            fan          : fields.fan,
//            idroom          : fields.idroom,
//        };
//        console.log(datainsert);
//        req.getConnection(function(err,connection){
//            var query = connection.query("UPDATE control_ac set ? WHERE id = ? ",[datainsert,id], function(err, rows)
//            {
//                if(err){
//                    var data = {
//                        result    : 'N',
//                        insert : datainsert
//
//                    };
//                    res.json({ data:data });
//                    console.log("Error Selecting : %s ",err );
//                }
//                else{
//                    var data = {
//                        result    : 'Y',
//                        insert : datainsert
//                    };
//                    res.json({ data:data });
//                }
//            });
//            //console.log(query.sql);
//        });
//    });
//};

exports.acDelete = function(req, res){
    var id = req.params.id;
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var datainsert = {
            flag          : 'N'
        };
        console.log(datainsert);
        req.getConnection(function(err,connection){
            var query = connection.query("UPDATE control_ac set ? WHERE id = ? ",[datainsert,id], function(err, rows)
            {
                if(err){
                    var data = {
                        result    : 'N',
                        insert : datainsert

                    };
                    res.json({ data:data });
                    console.log("Error Selecting : %s ",err );
                }
                else{
                    var data = {
                        result    : 'Y',
                        insert : datainsert
                    };
                    res.json({ data:data });
                }
            });
            //console.log(query.sql);
        });
    });
};
