var formidable = require('formidable');
var util = require('util');



exports.lightingAll = function(req, res){
var datax=[];
    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM lighting where flag="Y"',function(err,roomL)
        {
            if(err){
                console.log("Error Selecting : %s ",err );
            }
            else{
                for(var s = 0;s < roomL.length;s++){
                    datax[s]={'id':roomL[s].idlamp,
                        'status':roomL[s].status,
                        'id Room':roomL[s].idroom
                    };
                }
                res.json({ lighting:datax });
            }
        });
    });
};

exports.lightingRoom = function(req, res){
    var datax=[];

    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
            var idroom  = fields.idroom;

    req.getConnection(function(err,connection){
        var query = connection.query('SELECT * FROM lighting where flag="Y" and idroom= ?',idroom,function(err,roomL)
        {
            if(err){
                console.log("Error Selecting : %s ",err );
            }
            else{
                for(var s = 0;s < roomL.length;s++){
                    datax[s]={'id':roomL[s].idlamp,
                        'status':roomL[s].status,
                        'id Room':roomL[s].idroom
                    };
                }
                res.json({ lighting:datax });
            }
        });
    });
    });
};
//
//exports.lightingSave = function(req, res){
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var datainsert = {
//            level          : fields.level,
//            idroom          : fields.idroom,
//            flag          : 'Y'
//
//        };
//        console.log(datainsert);
//        req.getConnection(function(err,connection){
//            var query = connection.query("INSERT INTO control_light set ? ",datainsert, function(err, rows)
//            {
//                if(err){
//                    var data = {
//                        result    : 'N',
//                        insert : datainsert
//
//                    };
//                            res.json({ data:data });
//                    console.log("Error Selecting : %s ",err );
//                }
//                else{
//                    var data = {
//                        result    : 'Y',
//                        insert : datainsert
//                    };
//                            res.json({ data:data });
//                }
//            });
//            //console.log(query.sql);
//        });
//    });
//};

exports.lightingEditSave = function(req, res){
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var id = fields.idroom;
        var datainsert = {
            level          : fields.level,
        };
        console.log(datainsert);
        req.getConnection(function(err,connection){
            var query = connection.query("UPDATE control_light set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
            {
                if(err){
                    var data = {
                        result    : 'N',
                        insert : datainsert

                    };
                    res.json({ data:data });
                    console.log("Error Selecting : %s ",err );
                }
                else{
                    var idroomv = fields.idroom;
                    if (fields.level==1){
                        var datainsertlamp = {
                            status          : '1'
                        };
                        var query = connection.query("UPDATE lighting set ? WHERE high='1' and idroom = ? ",[datainsertlamp,idroomv], function(err, rows)
                        {
                            if(err){
                                var data = {
                                    result    : 'N',
                                    insert : datainsert

                                };
                                res.json({ data:data });
                                console.log("Error Selecting : %s ",err );
                            }
                            else{
                                var datainsertlamp2 = {
                                    status          : '0'
                                };

                                var query = connection.query("UPDATE lighting set ? WHERE high!='1' and idroom = ? ",[datainsertlamp2,idroomv], function(err, rows)
                                {
                                    if(err){
                                        var data = {
                                            result    : 'N',
                                            insert : datainsert

                                        };
                                        res.json({ data:data });
                                        console.log("Error Selecting : %s ",err );
                                    }
                                    else{
                                        var data = {
                                            result    : 'Y',
                                            insert : datainsert
                                        };
                                        res.json({ data:data });
                                    }
                                });
                            }
                        });
                    }
                    else if (fields.level==2){
                        var datainsertlamp = {
                            status          : '1'
                        };
                        var query = connection.query("UPDATE lighting set ? WHERE medium='1' and idroom = ? ",[datainsertlamp,idroomv], function(err, rows)
                        {
                            if(err){
                                var data = {
                                    result    : 'N',
                                    insert : datainsert

                                };
                                res.json({ data:data });
                                console.log("Error Selecting : %s ",err );
                            }
                            else{
                                var datainsertlamp2 = {
                                    status          : '0'
                                };

                                var query = connection.query("UPDATE lighting set ? WHERE medium!='1' and idroom = ? ",[datainsertlamp2,idroomv], function(err, rows)
                                {
                                    if(err){
                                        var data = {
                                            result    : 'N',
                                            insert : datainsert

                                        };
                                        res.json({ data:data });
                                        console.log("Error Selecting : %s ",err );
                                    }
                                    else{
                                        var data = {
                                            result    : 'Y',
                                            insert : datainsert
                                        };
                                        res.json({ data:data });
                                    }
                                });
                            }
                        });
                    }
                    else if (fields.level==3){
                        var datainsertlamp = {
                            status          : '1'
                        };
                        var query = connection.query("UPDATE lighting set ? WHERE low='1' and idroom = ? ",[datainsertlamp,idroomv], function(err, rows)
                        {
                            if(err){
                                var data = {
                                    result    : 'N',
                                    insert : datainsert

                                };
                                res.json({ data:data });
                                console.log("Error Selecting : %s ",err );
                            }
                            else{
                                var datainsertlamp2 = {
                                    status          : '0'
                                };

                                var query = connection.query("UPDATE lighting set ? WHERE low!='1' and idroom = ? ",[datainsertlamp2,idroomv], function(err, rows)
                                {
                                    if(err){
                                        var data = {
                                            result    : 'N',
                                            insert : datainsert

                                        };
                                        res.json({ data:data });
                                        console.log("Error Selecting : %s ",err );
                                    }
                                    else{
                                        var data = {
                                            result    : 'Y',
                                            insert : datainsert
                                        };
                                        res.json({ data:data });
                                    }
                                });
                            }
                        });
                    }
                    else if (fields.level==0){
                        var datainsertlamp = {
                            status          : '0'
                        };
                        var query = connection.query("UPDATE lighting set ? WHERE  idroom = ? ",[datainsertlamp,idroomv], function(err, rows)
                        {
                            if(err){
                                var data = {
                                    result    : 'N',
                                    insert : datainsert

                                };
                                res.json({ data:data });
                                console.log("Error Selecting : %s ",err );
                            }
                            else{
                                var data = {
                                result    : 'Y',
                                insert : datainsert
                            };
                                res.json({ data:data });
                            }
                        });
                    }

                }
            });
            //console.log(query.sql);
        });
    });
};

//exports.lightingPowerEditSave = function(req, res){
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var id = fields.idroom;
//        if (fields.status==0){
//            var datainsert = {
//                status          : '0'
//            };
//            console.log(datainsert);
//            req.getConnection(function(err,connection){
//                var query = connection.query("UPDATE lighting set ? WHERE idroom = ? ",[datainsert,id], function(err, rows)
//                {
//                    if(err){
//                        var data = {
//                            result    : 'N',
//                            insert : datainsert
//                        };
//                        res.json({ data:data });
//                        console.log("Error Selecting : %s ",err );
//                    }
//                    else{
//                        var data = {
//                            result    : 'Y',
//                            insert : datainsert
//                        };
//                        res.json({ data:data });
//                    }
//                });
//                //console.log(query.sql);
//            });
//        }
//        else {
//            var id = fields.idroom;
//            req.getConnection(function(err,connection){
//                var query = connection.query('SELECT * FROM control_light where flag="Y" and idroom= ?',id,function(err,roomL)
//                {
//                    if(err){
//                        console.log("Error Selecting : %s ",err );
//                    }
//                    else{
//                        level = roomL[0].level;
//                        if (level==1){
//                            var datainsertlamp = {
//                                status          : '1'
//                            };
//                            var query = connection.query("UPDATE lighting set ? WHERE high='1' and idroom = ? ",[datainsertlamp,id], function(err, rows)
//                            {
//                                if(err){
//                                    var data = {
//                                        result    : 'N',
//                                        insert : datainsert
//
//                                    };
//                                    res.json({ data:data });
//                                    console.log("Error Selecting : %s ",err );
//                                }
//                                else{
//                                        var datainsertlamp2 = {
//                                            status          : '0'
//                                        };
//
//                                    var query = connection.query("UPDATE lighting set ? WHERE high!='1' and idroom = ? ",[datainsertlamp2,id], function(err, rows)
//                                    {
//                                        if(err){
//                                            var data = {
//                                                result    : 'N',
//                                                insert : datainsert
//
//                                            };
//                                            res.json({ data:data });
//                                            console.log("Error Selecting : %s ",err );
//                                        }
//                                        else{
//                                            var data = {
//                                                result    : 'Y',
//                                                insert : datainsert
//                                            };
//                                            res.json({ data:data });
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                        else if (level==2){
//                            var datainsertlamp = {
//                                status          : '1'
//                            };
//                            var query = connection.query("UPDATE lighting set ? WHERE medium='1' and idroom = ? ",[datainsertlamp,id], function(err, rows)
//                            {
//                                if(err){
//                                    var data = {
//                                        result    : 'N',
//                                        insert : datainsert
//
//                                    };
//                                    res.json({ data:data });
//                                    console.log("Error Selecting : %s ",err );
//                                }
//                                else{
//                                        var datainsertlamp2 = {
//                                            status          : '0'
//                                        };
//
//                                    var query = connection.query("UPDATE lighting set ? WHERE medium!='1' and idroom = ? ",[datainsertlamp2,id], function(err, rows)
//                                    {
//                                        if(err){
//                                            var data = {
//                                                result    : 'N',
//                                                insert : datainsert
//
//                                            };
//                                            res.json({ data:data });
//                                            console.log("Error Selecting : %s ",err );
//                                        }
//                                        else{
//                                            var data = {
//                                                result    : 'Y',
//                                                insert : datainsert
//                                            };
//                                            res.json({ data:data });
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                        else if (level==3){
//                            var datainsertlamp = {
//                                status          : '1'
//                            };
//                            var query = connection.query("UPDATE lighting set ? WHERE low='1' and idroom = ? ",[datainsertlamp,id], function(err, rows)
//                            {
//                                if(err){
//                                    var data = {
//                                        result    : 'N',
//                                        insert : datainsert
//
//                                    };
//                                    res.json({ data:data });
//                                    console.log("Error Selecting : %s ",err );
//                                }
//                                else{
//                                        var datainsertlamp2 = {
//                                            status          : '0'
//                                        };
//
//                                    var query = connection.query("UPDATE lighting set ? WHERE low!='1' and idroom = ? ",[datainsertlamp2,id], function(err, rows)
//                                    {
//                                        if(err){
//                                            var data = {
//                                                result    : 'N',
//                                                insert : datainsert
//
//                                            };
//                                            res.json({ data:data });
//                                            console.log("Error Selecting : %s ",err );
//                                        }
//                                        else{
//                                            var data = {
//                                                result    : 'Y',
//                                                insert : datainsert
//                                            };
//                                            res.json({ data:data });
//                                        }
//                                    });
//                                }
//                            });
//                        }
//                    }
//                });
//            });
//        }
//
//    });
//};

//exports.lightingEditSave = function(req, res){
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var id = fields.id;
//        var datainsert = {
//            status          : fields.status,
//            level          : fields.level,
//            idroom          : fields.idroom
//        };
//        console.log(datainsert);
//        req.getConnection(function(err,connection){
//            var query = connection.query("UPDATE control_light set ? WHERE idcl = ? ",[datainsert,id], function(err, rows)
//            {
//                if(err){
//                    var data = {
//                        result    : 'N',
//                        insert : datainsert
//
//                    };
//                    res.json({ data:data });
//                    console.log("Error Selecting : %s ",err );
//                }
//                else{
//                    if (fields.level==1){
//                        var idroom = fields.idroom;
//                        var datainsertlamp = {
//                            status          : fields.status
//                        };
//                        var query = connection.query("UPDATE lighting set ? WHERE high='1' and idroom = ? ",[datainsertlamp,idroom], function(err, rows)
//                        {
//                            if(err){
//                                var data = {
//                                    result    : 'N',
//                                    insert : datainsert
//
//                                };
//                                res.json({ data:data });
//                                console.log("Error Selecting : %s ",err );
//                            }
//                            else{
//
//
//                                var data = {
//                                    result    : 'Y',
//                                    insert : datainsert
//                                };
//                                res.json({ data:data });
//                            }
//                        });
//                    }
//                    else if (fields.level==2){
//                        console.log('haaii');
//                        var idroom = fields.idroom;
//                        var datainsertlamp = {
//                            status          : fields.status
//                        };
//                        var query = connection.query("UPDATE lighting set ? WHERE medium='1' and idroom = ? ",[datainsertlamp,idroom], function(err, rows)
//                        {
//                            if(err){
//                                var data = {
//                                    result    : 'N',
//                                    insert : datainsert
//
//                                };
//                                res.json({ data:data });
//                                console.log("Error Selecting : %s ",err );
//                            }
//                            else{
//
//
//                                var data = {
//                                    result    : 'Y',
//                                    insert : datainsert
//                                };
//                                res.json({ data:data });
//                            }
//                        });
//                    }
//                    else if (fields.level==3){
//                        var idroom = fields.idroom;
//                        var datainsertlamp = {
//                            status          : fields.status
//                        };
//                        var query = connection.query("UPDATE lighting set ? WHERE low='1' and idroom = ? ",[datainsertlamp,idroom], function(err, rows)
//                        {
//                            if(err){
//                                var data = {
//                                    result    : 'N',
//                                    insert : datainsert
//
//                                };
//                                res.json({ data:data });
//                                console.log("Error Selecting : %s ",err );
//                            }
//                            else{
//
//
//                                var data = {
//                                    result    : 'Y',
//                                    insert : datainsert
//                                };
//                                res.json({ data:data });
//                            }
//                        });
//                    }
//
//                }
//            });
//            //console.log(query.sql);
//        });
//    });
//};
//
//exports.lightingDelete = function(req, res){
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var id = fields.id;
//        var datainsert = {
//            flag          : 'N'
//        };
//        console.log(datainsert);
//        req.getConnection(function(err,connection){
//            var query = connection.query("UPDATE lighting set ? WHERE idlamp = ? ",[datainsert,id], function(err, rows)
//            {
//                if(err){
//                    var data = {
//                        result    : 'N',
//                        insert : datainsert
//
//                    };
//                    res.json({ data:data });
//                    console.log("Error Selecting : %s ",err );
//                }
//                else{
//                    var data = {
//                        result    : 'Y',
//                        insert : datainsert
//                    };
//                    res.json({ data:data });
//                }
//            });
//            //console.log(query.sql);
//        });
//    });
//};
//
//exports.lightingPower = function(req, res){
//    var form = new formidable.IncomingForm();
//    form.parse(req, function(err, fields, files) {
//        var id = fields.id;
//        var datainsert = {
//            flag          : 'N'
//        };
//        console.log(datainsert);
//        req.getConnection(function(err,connection){
//            var query = connection.query("UPDATE lighting set ? WHERE idlamp = ? ",[datainsert,id], function(err, rows)
//            {
//                if(err){
//                    var data = {
//                        result    : 'N',
//                        insert : datainsert
//
//                    };
//                    res.json({ data:data });
//                    console.log("Error Selecting : %s ",err );
//                }
//                else{
//                    var data = {
//                        result    : 'Y',
//                        insert : datainsert
//                    };
//                    res.json({ data:data });
//                }
//            });
//            //console.log(query.sql);
//        });
//    });
//};