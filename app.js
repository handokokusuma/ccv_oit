/**
 * Module dependencies.
 */

var express = require('express');
var bodyParser = require('body-parser');
var form = require("express-form");


var routes = require('./routes');
var http = require('http');
var path = require('path');

//load customers route

var menu = require('./routes/menu');
var room = require('./routes/room');
var lighting = require('./routes/lighting');
var ac = require('./routes/ac');


var app = express();
var connection  = require('express-myconnection');
var mysql = require('mysql');

// all environments
app.set('port', process.env.PORT || 8083);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

/*------------------------------------------
 connection peer, register as middleware
 type koneksi : single,pool and request
 -------------------------------------------*/

app.use(

    connection(mysql,{

        host: 'prod2.colekbayar.com',
        user: 'ccv',
        password : 'P@ssw0rd',
        port : 3306, //port mysql
        database:'ccv_iot'

    },'pool') //or single

);



app.get('/', routes.index);
//ussd
app.post('/menu', menu.menuAll);
app.post('/listRoom', room.roomAll);
app.post('/roomitem', room.roomItem);
app.post('/room/save', room.roomSave);

app.post('/lighting', lighting.lightingAll);
app.post('/lighting/room', lighting.lightingRoom);
//app.post('/lighting/save', lighting.lightingSave);
app.post('/lighting/edit', lighting.lightingEditSave);
//app.post('/lighting/power', lighting.lightingPowerEditSave);
//app.post('/lighting/delete', lighting.lightingDelete);



app.post('/ac', ac.acAll);
app.post('/ac/save', ac.acSave);
app.post('/ac/show/:id', ac.acShow);
app.post('/ac/edit', ac.acEditSave);
app.post('/ac/delete/:id', ac.acDelete);









app.use(app.router);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
